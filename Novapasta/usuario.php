<?php
session_start();
include("verifica-logado.php");
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Cadastro de Novo Usuário</title>
    <?php include("links-css-js.php"); ?>
</head>

<body>

    <div class="container">
    <?php include("menu.php"); 
        if(isset($_SESSION['imagem_ok'])){
            print "
                <br />
                <div class='alert alert-primary' role='alert'>".$_SESSION['imagem_ok']."
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'> 
                <span aria-hidden='true'>$times</span></button> </div>
            ";
        }
    
    ?>

    <h3 class="mt-3 mb-3">Pagina do Usuario <?php print $_SESSION['nome_usuario']; ?></h3>

       <div class="row mt-5">
           <div class="col-sm-4">
                <?php 
                print "
                <img src='".$_SESSION['foto_usuario']."' alt='".$_SESSION['nome_usuario']."' title='".$_SESSION['nome_usuario']."' width='100%' class='img-thumbnail' />                ";

                ?>


           </div>
           <div class="col-sm-8">
                <h3 class="mt-3">Nome:</h3>
                <?php print $_SESSION['nome_usuario']; ?>
                <hr />
                <hr />
                <h3>E-mail:</h3>
                <?php print $_SESSION['email_usuario']; ?>
                <hr />
                <hr />
                <h3>Status:</h3>
                Usuario ativo no sistema


           </div>
    </div>

    <div class="row mt-4 mb-4">

        <div class="col-sm">
            <form name="frm_imagem" id="frm_imagem" action="upload.php" method="post" enctype="multipart/form-data">
                 <h4>Modificar Imagem do Usuário</h4>
                 <div class="custom-file">
                     <input type="file" class="custom-file-input" id="imagem" name="imagem" />
                     <label class="custom-file-label" for="imagem">Escolha uma imagem</label>
                 </div>
                 <div class="mt-3">
                     <input type="submit" name="enviar" id="enviar" value="Enviar" class="btn btn-primary w-100">
                 </div>
            </form>
        </div> 

        <div class="col-sm">

            <h4>Modificar os dados do Usuário</h4>

        </div>


    </div>

          

<script>
$(".custom-file-input").on("change", function() {
var filename = $(this).val().split("\\").pop();
$(this).siblings(".custom-file-label").addClass("selected").html(filename);

});
</script>
</body>

</html>