<?php
session_start();
include("funcao.php");
$pasta_local = "upload/";
$codigo = LimpaString($_POST['cod1']);
$caminho_imagem = $pasta_local . basename($_FILES["imagem_categoria"]["name"]);
$uploadok = 1;
$tipo_imagem = strtolower(pathinfo($caminho_imagem, PATHINFO_EXTENSION));
$novo_nome = $pasta_local . "c" . $codigo . "." . $tipo_imagem;

// verificar se é uma imagem ou não
if (isset($_POST['enviar'])) {
    $check = getimagesize($_FILES['imagem_categoria']['tmp_name']);
    if ($check !== false) {
        $uploadok = 1;
    } else {
        print "Arquivo não é uma imagem. <a href='usuario.php'>voltar</a>";
        $uploadok = 0;
    }
}

// verificar se o arquivo já existe
/* if (file_exists($caminho_imagem)) {
    print "Erro, arquivo já existente no servidor. <a href='usuario.php'>voltar</a>";
    $uploadok = 0;
}
*/

// verificar o tamanho do arquivo => 1024KB = 1MB
if ($_FILES["imagem_categoria"]["size"] > 100000000) {
    print "Erro, arquivo muito grande. <a href='usuario.php'>voltar</a>";
    $uploadok = 0;
}

// verificar o tipo da imagem permitido
if ($tipo_imagem != "jpg" && $tipo_imagem != "png" && $tipo_imagem != "jpeg") {
    print "Erro, permitido somente arquivos no formato jpg, png e jpeg. <a href='usuario.php'>voltar</a>";
    $uploadok = 0;
}

if ($uploadok == 0) {
    print "Erro, seu arquivo não foi aceito. <a href='usuario.php'>voltar</a>";
} else {
    if (move_uploaded_file($_FILES["imagem_categoria"]["tmp_name"], $novo_nome)) {
        include("conecta.php");
        $sql = "UPDATE tb_categoria
           SET imagem_categoria = '$novo_nome'
           WHERE cod_categoria = '" . $codigo . "'          
           ";
        $res = mysqli_query($_con, $sql) or die("Não foi possível atualizar o endereço da imagem");
        $_SESSION['imagem_ok'] = "O arquivo " . basename($_FILES["imagem_categoria"]["name"]) . " foi atualizado";
        header("location: editar-categoria.php?cod=$codigo");
    } else {

        print "Erro, não possível fazer o upload. <a href='usuario.php'>voltar</a>";
    }
}

