-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 16-Mar-2020 às 21:16
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bd_videos`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categoria`
--

CREATE TABLE `tb_categoria` (
  `cod_categoria` int(11) NOT NULL,
  `nome_categoria` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `imagem_categoria` varchar(300) COLLATE latin1_general_ci NOT NULL,
  `data_categoria` date NOT NULL,
  `hora_categoria` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_categoria`
--

INSERT INTO `tb_categoria` (`cod_categoria`, `nome_categoria`, `imagem_categoria`, `data_categoria`, `hora_categoria`) VALUES
(2, 'Notícias do Brasil', 'upload/c2.png', '2020-03-05', '14:59:10'),
(5, 'Música', 'upload/avatar.png', '2020-03-05', '15:08:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuario`
--

CREATE TABLE `tb_usuario` (
  `cod_usuario` int(11) NOT NULL,
  `nome_usuario` varchar(100) NOT NULL,
  `email_usuario` varchar(100) NOT NULL,
  `senha_usuario` varchar(100) NOT NULL,
  `foto_usuario` varchar(300) NOT NULL,
  `data_usuario` date NOT NULL,
  `hora_usuario` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `tb_usuario`
--

INSERT INTO `tb_usuario` (`cod_usuario`, `nome_usuario`, `email_usuario`, `senha_usuario`, `foto_usuario`, `data_usuario`, `hora_usuario`) VALUES
(1, 'Larissa Gabriele', 'larigabriele2511@gmail.com', '$2y$10$HUpkqnsDHJP4sswcFNM0LOPQQys4BaoDnRRLTFRA6aVx7VwdvVAmS', 'upload/1.png', '2020-03-10', '17:21:52'),
(2, 'gustavo', 'gustavo@gmail.com', '$2y$10$Ow9rM3Hbo7tnz2VZTjy3heNrAHSxCzUjwwTL9sAdlpODrn1L2tYAi', 'upload/avatar.png', '2020-03-11', '15:30:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_videos`
--

CREATE TABLE `tb_videos` (
  `cod_videos` int(11) NOT NULL,
  `cod_categoria` int(11) NOT NULL,
  `nome_videos` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `id_videos` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `data_videos` date NOT NULL,
  `hora_videos` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_videos`
--

INSERT INTO `tb_videos` (`cod_videos`, `cod_categoria`, `nome_videos`, `id_videos`, `data_videos`, `hora_videos`) VALUES
(2, 2, 'NEYMAR, MBAPPÉ E PSG NA FINAL! ', '0zmJ735bKBM', '2020-03-09', '14:23:02'),
(3, 5, 'Audioslave - Show Me How to Live', 'vVXIK1xCRpY', '2020-03-09', '16:36:00'),
(4, 5, 'girassóis de van gogh - baco exu do blues || cover', 'cRLSKh5ridA', '2020-03-10', '14:38:19');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `tb_categoria`
--
ALTER TABLE `tb_categoria`
  ADD PRIMARY KEY (`cod_categoria`);

--
-- Índices para tabela `tb_usuario`
--
ALTER TABLE `tb_usuario`
  ADD PRIMARY KEY (`cod_usuario`);

--
-- Índices para tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  ADD PRIMARY KEY (`cod_videos`),
  ADD KEY `cod_categoria` (`cod_categoria`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `tb_categoria`
--
ALTER TABLE `tb_categoria`
  MODIFY `cod_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `tb_usuario`
--
ALTER TABLE `tb_usuario`
  MODIFY `cod_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  MODIFY `cod_videos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `tb_videos`
--
ALTER TABLE `tb_videos`
  ADD CONSTRAINT `tb_videos_ibfk_1` FOREIGN KEY (`cod_categoria`) REFERENCES `tb_categoria` (`cod_categoria`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
