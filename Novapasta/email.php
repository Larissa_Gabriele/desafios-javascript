<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Formulário de Contato</title>
    <?php include("links-css-js.php"); ?>
</head>

<body>
    <?php include("menu.php"); ?>

    <div class="container">

        <fieldset class="mt-5">
            <legend> Entre com os dados para contato </legend>

            <form id="frm_mail" name="frm_mail" method="post" action="valida-email.php">

                <p>Entre com o seu nome: <br>
                    <input id="nome" type="text" name="nome" required placeholder="Entre com um nome para contato" />
                </p>
                <p>Entre com o E-mail: <br>
                    <input id="email" type="email" name="email" required placeholder="Entre com o e-mail" />
                </p>

                <p>Entre com o Telefone:<br>
                    <input id="telefone" type="text" name="telefone" required placeholder="Entre com o telefone para contato" />
                </p>

                <p>
                    <input id="assunto" type="text" name="assunto" required placeholder="Entre com o assunto do e-mail" />
                </p>

                Entre com sua mensagem:<br>
                <textarea name="mensagem" id="mensagem" required></textarea>

                <input type="submit" name="enviar" id="enviar" value="Enviar" />




            </form>

        </fieldset>




    </div>


</body>

</html>
